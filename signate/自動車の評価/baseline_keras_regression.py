# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np

np.random.seed(2022)
# -

# データ読み込み
train = pd.read_csv('train.tsv', sep="\t")
test = pd.read_csv('test.tsv', sep="\t")

# +
# id行を削除
train = train.drop("id", axis=1)

# classの数値化
train["class"][train["class"]=="unacc"] = 0
train["class"][train["class"]=="acc"] = 1
train["class"][train["class"]=="good"] = 2
train["class"][train["class"]=="vgood"] = 3

# buyingの数値化
train["buying"][train["buying"]=="low"] = 1
train["buying"][train["buying"]=="med"] = 2
train["buying"][train["buying"]=="high"] = 3
train["buying"][train["buying"]=="vhigh"] = 4

# maintの数値化
train["maint"][train["maint"]=="low"] = 1
train["maint"][train["maint"]=="med"] = 2
train["maint"][train["maint"]=="high"] = 3
train["maint"][train["maint"]=="vhigh"] = 4

# doorsの数値化
train["doors"][train["doors"]=="2"] = 1
train["doors"][train["doors"]=="3"] = 2
train["doors"][train["doors"]=="4"] = 3
train["doors"][train["doors"]=="5more"] = 4

# personsの数値化
train["persons"][train["persons"]=="2"] = 1
train["persons"][train["persons"]=="4"] = 2
train["persons"][train["persons"]=="more"] = 3

# lug_bootの数値化
train["lug_boot"][train["lug_boot"]=="small"] = 1
train["lug_boot"][train["lug_boot"]=="med"] = 2
train["lug_boot"][train["lug_boot"]=="big"] = 3

# safetyの数値化
train["safety"][train["safety"]=="low"] = 1
train["safety"][train["safety"]=="med"] = 2
train["safety"][train["safety"]=="high"] = 3


train = train.astype(np.int64)
y = train["class"]
train = train.drop("class", axis=1)
# 数値型変数の確認
train.describe()

# +
# id行を削除
test = test.drop("id", axis=1)

# buyingの数値化
test["buying"][test["buying"]=="low"] = 1
test["buying"][test["buying"]=="med"] = 2
test["buying"][test["buying"]=="high"] = 3
test["buying"][test["buying"]=="vhigh"] = 4

# maintの数値化
test["maint"][test["maint"]=="low"] = 1
test["maint"][test["maint"]=="med"] = 2
test["maint"][test["maint"]=="high"] = 3
test["maint"][test["maint"]=="vhigh"] = 4

# doorsの数値化
test["doors"][test["doors"]=="2"] = 1
test["doors"][test["doors"]=="3"] = 2
test["doors"][test["doors"]=="4"] = 3
test["doors"][test["doors"]=="5more"] = 4

# personsの数値化
test["persons"][test["persons"]=="2"] = 1
test["persons"][test["persons"]=="4"] = 2
test["persons"][test["persons"]=="more"] = 3

# lug_bootの数値化
test["lug_boot"][test["lug_boot"]=="small"] = 1
test["lug_boot"][test["lug_boot"]=="med"] = 2
test["lug_boot"][test["lug_boot"]=="big"] = 3

# safetyの数値化
test["safety"][test["safety"]=="low"] = 1
test["safety"][test["safety"]=="med"] = 2
test["safety"][test["safety"]=="high"] = 3

# 数値型変数の確認
test = test.astype(np.int64)
# -

train.head()

y.head()

test.head()

# +
# 標準化
from sklearn import preprocessing
sc = preprocessing.StandardScaler()
train = sc.fit_transform(train)

train.mean(axis=0), train.std(axis=0)

# +
# 標準化
# mean =  y.mean()
# std = y.std()
# y = (y - mean) / std
# y.mean(), y.std()

# +

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(train, y, 
                                                    test_size = .2,
                                                    random_state = 666,
                                                    stratify = y)
X_train.shape, y_train.shape

# +
import keras
from keras.layers import Dense, Dropout, Activation
import tensorflow as tf



model = keras.Sequential()
model.add(Dense(units=100, activation='relu', input_dim=6))
model.add(Dense(units=50, activation='relu',))
model.add(Dropout(0.2))
model.add(Dense(units=1, activation='sigmoid'))
model.summary()
# モデルのコンパイル
model.compile(
    loss='mean_squared_error',     # 多値分類
    optimizer=tf.keras.optimizers.Adam(lr=0.01),             # オプティマイザ（Adam最適化）
    metrics=['accuracy'])    # 評価関数
# model.add(Dense(64, activation="relu", input_shape=(X_train.shape[1],)))
# model.add(Dropout(0.2))
# model.add(Dense(64, activation="relu"))
# model.add(Dropout(0.2))
# model.add(Dense(1, activation='softmax'))
# model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), metrics=['accuracy'])
# model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(learning_rate=0.01), metrics=['accuracy'])
# -



batch_size = 128
epochs = 20
history = model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=(X_test, y_test))

# +
import matplotlib.pyplot as plt

# plt.plot(history.history['accuracy'])
plt.plot(history.history["accuracy"])
plt.plot(history.history['val_accuracy'])
# plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# -

plt.xlabel('epoch')
plt.ylabel('loss')
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
plt.show()

# +
pred = model.predict(test)
# pred = pred * y.std() + y.mean()
# pred = np.argmax(pred, axis=1)



pred[pred<0.5] = 0
pred[(0.5<pred)*(pred<1.5)] = 1
pred[(1.5<pred)*(pred<2.5)] = 2
pred[pred>2.5] = 3

pred = pred.astype("int")
pred = pred.astype("str")

pred[pred=="0"] = "unacc"
pred[pred=="1"] = "acc"
pred[pred=="2"] = "good"
pred[pred=="3"] = "vgood"

pred[:10]

# -

submission = pd.read_csv("./sample_submit.csv", header=None)
submission.iloc[:,1] = pred
submission.to_csv("submission.csv", index=None, header=None)


