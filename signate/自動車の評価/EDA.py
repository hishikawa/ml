# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
# 訓練データ読み込み
import pandas as pd
import numpy as np

train = pd.read_csv('train.tsv', sep="\t")
# -

# データサイズ確認
train.shape

# データのプレビュー
train.head()

# 訓練データ詳細表示
train.info()

# 数値型変数の確認
num_cols = [col for col in train.columns if train[col].dtype in ["int64", "float64"]]
train[num_cols].describe()

# カテゴリ変数の確認
cat_cols = [col for col in train.columns if train[col].dtype == 'O']
train[cat_cols].describe()

# +
# カテゴリ変数の個誘致を表示

for col in cat_cols:
    uniq = np.unique(train[col])
    print('-'*50)
    print("col {}, n_uniq {}, uniq{}".format(col, len(uniq), uniq))
# -

# 可視化
import matplotlib
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns

# 各変数を可視化
for col in cat_cols:
    print("col:", col)
    fig, ax = plt.subplots(figsize=(8, 6))
    sns.countplot(x=col, data=train)
    plt.show()

# id行を削除
train = train.drop("id", axis=1)

# classの数値化
train["class"][train["class"]=="unacc"] = 1
train["class"][train["class"]=="acc"] = 2
train["class"][train["class"]=="good"] = 3
train["class"][train["class"]=="vgood"] = 4

# buyingの数値化
train["buying"][train["buying"]=="low"] = 1
train["buying"][train["buying"]=="med"] = 2
train["buying"][train["buying"]=="high"] = 3
train["buying"][train["buying"]=="vhigh"] = 4

# maintの数値化
train["maint"][train["maint"]=="low"] = 1
train["maint"][train["maint"]=="med"] = 2
train["maint"][train["maint"]=="high"] = 3
train["maint"][train["maint"]=="vhigh"] = 4

# doorsの数値化
train["doors"][train["doors"]=="2"] = 1
train["doors"][train["doors"]=="3"] = 2
train["doors"][train["doors"]=="4"] = 3
train["doors"][train["doors"]=="5more"] = 4

# personsの数値化
train["persons"][train["persons"]=="2"] = 1
train["persons"][train["persons"]=="4"] = 2
train["persons"][train["persons"]=="more"] = 3

# lug_bootの数値化
train["lug_boot"][train["lug_boot"]=="small"] = 1
train["lug_boot"][train["lug_boot"]=="med"] = 2
train["lug_boot"][train["lug_boot"]=="big"] = 3

# safetyの数値化
train["safety"][train["safety"]=="low"] = 1
train["safety"][train["safety"]=="med"] = 2
train["safety"][train["safety"]=="high"] = 3

train.head()

# 数値型変数の確認
train = train.astype(np.int64)
train.describe()

# 各変数を可視化
for col in cat_cols:
    print("col:", col)
    fig, ax = plt.subplots(figsize=(8, 6))
    sns.countplot(x=col, data=train, hue="class")
    plt.show()

sns.pairplot(data=train, hue="class")
plt.show()

#散布図とヒストグラムを併せて表示する
sns.jointplot(x='class', y='persons', data=train, kind='hex')
plt.show()

cor = train.corr()
cor

sns.heatmap(cor, annot=True, fmt='.2f')
plt.show()


