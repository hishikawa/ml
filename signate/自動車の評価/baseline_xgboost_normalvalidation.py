# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import pandas as pd
import numpy as np
import xgboost as xgb

np.random.seed(2022)
# -

# データ読み込み
train = pd.read_csv('train.tsv', sep="\t")
test = pd.read_csv('test.tsv', sep="\t")

# +
# id行を削除
train = train.drop("id", axis=1)

# classの数値化
train["class"][train["class"]=="unacc"] = 1
train["class"][train["class"]=="acc"] = 2
train["class"][train["class"]=="good"] = 3
train["class"][train["class"]=="vgood"] = 4

# buyingの数値化
train["buying"][train["buying"]=="low"] = 1
train["buying"][train["buying"]=="med"] = 2
train["buying"][train["buying"]=="high"] = 3
train["buying"][train["buying"]=="vhigh"] = 4

# maintの数値化
train["maint"][train["maint"]=="low"] = 1
train["maint"][train["maint"]=="med"] = 2
train["maint"][train["maint"]=="high"] = 3
train["maint"][train["maint"]=="vhigh"] = 4

# doorsの数値化
train["doors"][train["doors"]=="2"] = 1
train["doors"][train["doors"]=="3"] = 2
train["doors"][train["doors"]=="4"] = 3
train["doors"][train["doors"]=="5more"] = 4

# personsの数値化
train["persons"][train["persons"]=="2"] = 1
train["persons"][train["persons"]=="4"] = 2
train["persons"][train["persons"]=="more"] = 3

# lug_bootの数値化
train["lug_boot"][train["lug_boot"]=="small"] = 1
train["lug_boot"][train["lug_boot"]=="med"] = 2
train["lug_boot"][train["lug_boot"]=="big"] = 3

# safetyの数値化
train["safety"][train["safety"]=="low"] = 1
train["safety"][train["safety"]=="med"] = 2
train["safety"][train["safety"]=="high"] = 3

# 数値型変数の確認
train = train.astype(np.int64)

y = train["class"]
train = train.drop("class", axis=1)

# +
# id行を削除
test = test.drop("id", axis=1)

# buyingの数値化
test["buying"][test["buying"]=="low"] = 1
test["buying"][test["buying"]=="med"] = 2
test["buying"][test["buying"]=="high"] = 3
test["buying"][test["buying"]=="vhigh"] = 4

# maintの数値化
test["maint"][test["maint"]=="low"] = 1
test["maint"][test["maint"]=="med"] = 2
test["maint"][test["maint"]=="high"] = 3
test["maint"][test["maint"]=="vhigh"] = 4

# doorsの数値化
test["doors"][test["doors"]=="2"] = 1
test["doors"][test["doors"]=="3"] = 2
test["doors"][test["doors"]=="4"] = 3
test["doors"][test["doors"]=="5more"] = 4

# personsの数値化
test["persons"][test["persons"]=="2"] = 1
test["persons"][test["persons"]=="4"] = 2
test["persons"][test["persons"]=="more"] = 3

# lug_bootの数値化
test["lug_boot"][test["lug_boot"]=="small"] = 1
test["lug_boot"][test["lug_boot"]=="med"] = 2
test["lug_boot"][test["lug_boot"]=="big"] = 3

# safetyの数値化
test["safety"][test["safety"]=="low"] = 1
test["safety"][test["safety"]=="med"] = 2
test["safety"][test["safety"]=="high"] = 3

# 数値型変数の確認
test = test.astype(np.int64)
# -

train.head()

y.head()

test.head()

# +
# from xgboost import XGBRFClassifier
# from sklearn.model_selection import train_test_split
# X_train, X_test, y_train, y_test = train_test_split(train, y, 
#                                                     test_size = .2,
#                                                     random_state = 666,
#                                                     stratify = y)
# -

from sklearn.model_selection import cross_val_score, StratifiedKFold
from xgboost import XGBRFClassifier
clf = XGBRFClassifier()
scores=cross_val_score(clf, train, y, cv=StratifiedKFold(n_splits=5))
print(scores.mean())
# cls.fit(X_train, y_train)



# +
# cls.score(X_train, y_train)

# +
clf.fit(train, y)
pred = clf.predict(test)

pred = pred.astype("str")
pred[pred=="1"] = "unacc"
pred[pred=="2"] = "acc"
pred[pred=="3"] = "good"
pred[pred=="4"] = "vgood"

pred[:5]

# -

submission = pd.read_csv("./sample_submit.csv", header=None)
submission.iloc[:,1] = pred
submission.to_csv("submission.csv", index=None, header=None)


